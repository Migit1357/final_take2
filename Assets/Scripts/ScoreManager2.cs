﻿using UnityEngine;
using System.Collections;
using 	UnityEngine.UI;

public class ScoreManager2 : MonoBehaviour {

	public Text newScoreText;
	public Text newHighScoreText;
	
	public float scoreCounter;
	public float highScoreCounter;
	
	public float pointsPerSecond;
	public bool isScoreIncreasing;
	
	
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey(" New HighScore")) 
		{
			highScoreCounter = PlayerPrefs.GetFloat (" New HighScore");
		}
	}
	
	// Update is called once per frame
	
	
	void Update () {
		
		if (isScoreIncreasing)
		{
			scoreCounter += pointsPerSecond * Time.deltaTime;
		}
		
		if (scoreCounter > highScoreCounter)
		{
			highScoreCounter = scoreCounter;
			PlayerPrefs.SetFloat (" New HighScore", highScoreCounter);
		}
		
		newScoreText.text = "Score: " + Mathf.Round (scoreCounter);
		newHighScoreText.text = "High Score: " + Mathf.Round (highScoreCounter);
	}
	
	public void AddScore(int pointsToAdd) {
		scoreCounter += pointsToAdd;
		
	}
}
