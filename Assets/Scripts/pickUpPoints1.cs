﻿using UnityEngine;
using System.Collections;

public class pickUpPoints1 : MonoBehaviour {

	public int scoreToGive;

	private AudioSource coinSound;

	private ScoreManager2 theScoreManager;

	// Use this for initialization
	void Start () {
		theScoreManager = FindObjectOfType<ScoreManager2> ();

		coinSound = GameObject.Find ("CoinSound").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {

		if (other.gameObject.name == "player") 
		{
			coinSound.Play ();
			theScoreManager.AddScore(scoreToGive);
			gameObject.SetActive(false);
		}
	}

}
