﻿using UnityEngine;
using System.Collections;

public class PlatformDestroying : MonoBehaviour {

	public GameObject platformDestroyingPoint;


	// Use this for initialization
	void Start () {

		platformDestroyingPoint = GameObject.Find ("PlatformDestroyingPoint");

	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < platformDestroyingPoint.transform.position.x) 
		{
			//Destroy (gameObject);

			gameObject.SetActive(false);
		}


	}
}
