﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	public float speedMultiplier;

	public float speedIncreaseMilestone;
	private float speedMilestoneCount;

	public float jumpForce;

	public float jumpTime;
	private float jumpTimeCounter;

	private Rigidbody2D myRgidBody;

	public bool grounded;
	public LayerMask whatIsGround;
	public Transform groundCheck;
	public float groundCheckRadius;

	public AudioSource jumpSound;
	public AudioSource deathSound;

	//private Collider2D myCollider;

	private Animator myAnimator;

	public GameManager theGameManager;

	private float moveSpeedStore;
	private float speedMilestoneCountStore;
	private float speedIncreaseMilestoneStore;

	private bool stoppedJumping;
	private bool doubleJumping;

	// Use this for initialization
	void Start () {
		myRgidBody = GetComponent<Rigidbody2D> ();

		//myCollider = GetComponent<Collider2D> ();

		myAnimator = GetComponent<Animator> ();

		jumpTimeCounter = jumpTime;

		speedMilestoneCount = speedIncreaseMilestone;

		moveSpeedStore = moveSpeed;

		speedMilestoneCountStore = speedMilestoneCount;

		speedIncreaseMilestoneStore = speedIncreaseMilestone;

		stoppedJumping = true;
	}
	
	// Update is called once per frame
	void Update () {

		//grounded = Physics2D.IsTouchingLayers (myCollider, whatIsGround);

		grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

		if (transform.position.x > speedMilestoneCount) 
		{
			speedMilestoneCount += speedIncreaseMilestone;

			speedIncreaseMilestone = speedIncreaseMilestone * speedMultiplier;

			moveSpeed = moveSpeed * speedMultiplier;
		}


		myRgidBody.velocity = new Vector2 (moveSpeed , myRgidBody.velocity.y);

		if (Input.GetKeyDown(KeyCode.Space) ||Input.GetMouseButtonDown(0))
		    {
			if(grounded) {
				myRgidBody.velocity = new Vector2 (myRgidBody.velocity.x, jumpForce);
				stoppedJumping = false;
				jumpSound.Play();
			}
			if(!grounded && doubleJumping)
			{
				myRgidBody.velocity = new Vector2 (myRgidBody.velocity.x, jumpForce);
				stoppedJumping = false;
				doubleJumping = false;
				jumpSound.Play();
			}
		}

		if ((Input.GetKey (KeyCode.Space) || Input.GetMouseButton (0)) && !stoppedJumping) 
		{
			if(jumpTimeCounter > 0) 
			{
				myRgidBody.velocity = new Vector2 (myRgidBody.velocity.x, jumpForce);
				jumpTimeCounter -= Time.deltaTime;
			}

		}

		if(Input.GetKeyUp (KeyCode.Space) || Input.GetMouseButtonUp (0))
		   {
			jumpTimeCounter = 0;
			stoppedJumping = true;
		}
		if (grounded) 
		{
			jumpTimeCounter = jumpTime;
			doubleJumping = true;
		}


		myAnimator.SetFloat ("Speed", myRgidBody.velocity.x);
		myAnimator.SetBool ("Grounded", grounded);
	}
		void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "killbox") 
		{

			theGameManager.RestartGame();
			moveSpeed = moveSpeedStore;
			speedMilestoneCount = speedMilestoneCountStore;
			speedIncreaseMilestone = speedIncreaseMilestoneStore;
			deathSound.Play();

		}

	}
}
