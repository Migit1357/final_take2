﻿using UnityEngine;
using System.Collections;

public class GameManager1 : MonoBehaviour {

	public Transform platformGenerator;
	private Vector3 platformStartPoint;

	public PlayerController thePlayer;
	private Vector3 playerStartPoint;

	public PlatformDestroying [] platformList;

	private ScoreManager2 theScoreManager;

	public DeathMenu theDeathScreen;

	// Use this for initialization
	void Start () {
	
		platformStartPoint = platformGenerator.position;
		playerStartPoint = thePlayer.transform.position;

		theScoreManager = FindObjectOfType<ScoreManager2> ();
	}
	
	// Update is called once per frame

	void Update () {

	}

	public void RestartGame () {
		theScoreManager.isScoreIncreasing = false;
		thePlayer.gameObject.SetActive (false);

		theDeathScreen.gameObject.SetActive (true);

		//StartCoroutine ("RestartGameCo");
	}
	public void Reset()
	{
		theDeathScreen.gameObject.SetActive (false);
		platformList = FindObjectsOfType<PlatformDestroying> ();
		for (int i = 0; i < platformList.Length; i++) 
		{
			platformList[i].gameObject.SetActive (false);
		}
		thePlayer.transform.position = playerStartPoint;
		
		platformGenerator.position = platformStartPoint;
		thePlayer.gameObject.SetActive (true);
		
		theScoreManager.scoreCounter = 0;
		theScoreManager.isScoreIncreasing = true;

	}

	/*public IEnumerator RestartGameCo()
	{
		theScoreManager.isScoreIncreasing = false;
		thePlayer.gameObject.SetActive (false);
		yield return new WaitForSeconds (0.5f);
		platformList = FindObjectsOfType<PlatformDestroying> ();
		for (int i = 0; i < platformList.Length; i++) 
		{
			platformList[i].gameObject.SetActive (false);
		}
		thePlayer.transform.position = playerStartPoint;

		platformGenerator.position = platformStartPoint;
		thePlayer.gameObject.SetActive (true);

		theScoreManager.scoreCounter = 0;
		theScoreManager.isScoreIncreasing = true;


	}*/
}
