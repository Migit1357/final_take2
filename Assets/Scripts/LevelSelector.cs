﻿using UnityEngine;
using System.Collections;

public class LevelSelector : MonoBehaviour {

	
	public string easyLevel;
	public string mediumLevel;
	public string hardLevel;
	public string mainMenu;
	
	
	
	public void PlayGameEasy() {
		
		Application.LoadLevel (easyLevel);
		
	}

	public void PlayGameMedium() {
		
		Application.LoadLevel (mediumLevel);
		
	}

	public void PlayGameHard() {
		
		Application.LoadLevel (hardLevel);
		
	}
	
	public void QuitGame() {
		Application.LoadLevel(mainMenu);
	}
	

}
