﻿using UnityEngine;
using System.Collections;

public class pickUpPoints : MonoBehaviour {

	public int scoreToGive;

	private AudioSource coinSound;

	private ScoreManager theScoreManager;

	// Use this for initialization
	void Start () {
		theScoreManager = FindObjectOfType<ScoreManager> ();

		coinSound = GameObject.Find ("CoinSound").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {

		if (other.gameObject.name == "player") 
		{

			theScoreManager.AddScore(scoreToGive);
			gameObject.SetActive(false);
			if(coinSound.isPlaying)
			{
				coinSound.Stop ();
				coinSound.Play ();
			}
			else 
			{
				coinSound.Play ();
			}
		}
	}

}
