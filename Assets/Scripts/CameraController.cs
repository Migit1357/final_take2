﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public PlayerController thePlayer;

	private Vector3 lastPlayerPosistion;
	private float distanceToMove;

	// Use this for initialization
	void Start () {
		thePlayer = FindObjectOfType<PlayerController>();
		lastPlayerPosistion = thePlayer.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		distanceToMove = thePlayer.transform.position.x - lastPlayerPosistion.x;

		transform.position = new Vector3(transform.position.x + distanceToMove, transform.position.y, transform.position.z);

		lastPlayerPosistion = thePlayer.transform.position;
	}
}
